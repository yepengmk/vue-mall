const Koa = require('koa')
const app = new Koa()
const mongoose = require('mongoose')
// 引入connect
const { connect, initSchemas } = require('./database/init.js')

// 立即执行函数
;(async () => {
  await connect()
  initSchemas()
  const User = mongoose.model('User')
  let twoUser = new User({userName: 'jspang2', password: '123456'})
  twoUser.save().then(() => {
    console.log('插入成功')
  })

  let users = await User.findOne({}).exec()
  console.log('------------------')
  console.log(users)
  console.log('------------------')
})()

app.use(async (ctx) => {
  ctx.body = '<h1>hello koa2</h1>'
})

app.listen(3000, () => {
  console.log('[Server] starting at port: 3000')
})
