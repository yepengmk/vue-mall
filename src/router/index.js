import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/pages/Index'
import Register from '@/components/pages/Register'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    }
  ]
})
