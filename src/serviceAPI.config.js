const BASEURL = 'https://www.easy-mock.com/mock/5cfcbdc332bc35359d5eb584/VueMall/'
const URL = {
  getIndex: BASEURL + 'index',
  getGoodsInfo: BASEURL + 'getGoodInfo'
}

module.exports = URL
